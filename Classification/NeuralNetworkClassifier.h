/*
 * NeuralNetwork.h
 *
 *  Created on: Jan 18, 2011
 *      Author: rgreen
 */

#ifndef NEURALNETWORK_H_
#define NEURALNETWORK_H_

#include <sstream>
#include <set>

#include "Classifier.h"
#include "OPFClassifier.h"
#include "MTRand.h"

class NeuralNetworkClassifier : public Classifier {
	public:
		NeuralNetworkClassifier();
		NeuralNetworkClassifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b);
		NeuralNetworkClassifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > t);
		NeuralNetworkClassifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > t, Classifier* cl, int o=1, int nts = 500, int nhn = 200, double la = 100);
		virtual ~NeuralNetworkClassifier();

		bool loadWeights(std::string filename);
		bool saveWeights(std::string filename);

		void init();
		void load();
		void reset();
		void test();

		void addLoad(int busNumber, double amount){}
		void addLoad(std::vector<double> amounts){}

		void setDesiredAccuracy(double ds);
		void setLearningRate(double lr);
		void setMomentum(double p);
		void setNumHiddenNeurons(int nhn);
		void setNumTrainingStates(int nts);
		void setVerbose(bool v);
		void setNumOutputs(int no);

		double run(std::string curSolution, double& excess);
		double run(std::vector<double> curSolution, double& excess);
		double getTrainingTime();
		double getClassificationTime();

	private:
		double activationFunction(double x);
		double clampOutput(double x);
		double classify(std::string curSolution, double& excess);
		double classify(std::vector<int> curSolution, double& excess);
		double classify(std::vector<double> curSolution, double& excess);
		double getOutputErrorGradient(double desiredValue, double outputValue);
		double getHiddenErrorGradient(int j);

		double getSetAccuracy(std::vector< std::vector < double > >& input, std::vector< std::vector < double > >& output);
		double getSetMSE(std::vector< std::vector < double > >& input, std::vector< std::vector < double > >& output);

		void backPropagate(std::vector<double>& desiredOutput);
		void createNetwork();
		void initWeights();
		void feedForward(std::vector<double>& pattern);
		void generateTrainingData();
		void train();
		void updateWeights();


		int nInputs,nHidden, nOutputs,
			numTrainingStates, maxEpochs;
		bool trained, verbose;
		double lineAdjustment, learningRate, momentum;
		double desiredAccuracy, trainingSetMSE;
		double trainingTime;


		std::vector < double > 	inputNeurons, hiddenNeurons, outputNeurons,
								hiddenErrorGradients, outputErrorGradients;

		std::vector < std::vector < double> > 	trainingInput, trainingOutput,
												validationInput, validationOutput,
												generalizationInput, generalizationOutput,
												wInputHidden, wHiddenOutput,
												deltaInputHidden, deltaHiddenOutput;
		Classifier* classifier;
};

#endif /* NEURALNETWORK_H_ */
