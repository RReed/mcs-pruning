/*
 * ACOPruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include "ACOPruner.h"
#include <iostream>

using namespace std;
ACOPruner::ACOPruner(int np, int nt,  Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, double r, double t,bool ul)
         : Pruner(np, nt, g, l, o, p, ul){
    Init(np, nt, o, g, l, p, r, t, ul);
}

ACOPruner::ACOPruner(int np, int nt,  Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : Pruner(np, nt, g, l, o, p, ul){
    Init(np, nt, o, g, l,p, 0.998125, 0.0013744);
}

void ACOPruner::Reset(int np, int nt){
    Pruner::Reset(np, nt);
    clearVectors();

     initPMoneTime   = 0;
     initPopTime     = 0;
     updatePopTime   = 0;
     updatePMoneTime = 0;
     fitnessTime     = 0;
}

void ACOPruner::Init(int np, int nt,  Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, double r, double t, bool ul){
    Pruner::Init(np, nt, g, l, o, p, ul);
    rho 	= r;
    deltaT 	= t;

    initPMoneTime   = 0;
    initPopTime     = 0;
    updatePopTime   = 0;
    updatePMoneTime = 0;
    fitnessTime     = 0;
}

void ACOPruner::initPheromones(){
    pMones.clear();

    subTimer.startTimer();
    for(unsigned int i=0; i<gens.size(); i++){
        pMones.push_back(gens[i].getOutageRate());
    }
    if(useLines){
        for(unsigned int i=0; i<lines.size(); i++){
            pMones.push_back(lines[i].getOutageRate());
        }
    }
    subTimer.stopTimer();
    initPMoneTime += timer.getElapsedTime();
}
void ACOPruner::initPopulation(MTRand& mt){
    ants.clear();

    subTimer.startTimer();
    for(int p=0; p< Np; p++){
        Ant a;
        a.startingPos = mt.randInt(Nd-1);
        for(int i=0; i<Nd; i++){
            a.pos.push_back(0);
        }
        ants.push_back(a);
    }
    subTimer.stopTimer();
    initPopTime += subTimer.getElapsedTime();
}
void ACOPruner::updateSolutions(MTRand& mt){
    int curPos, nextPos;
    double r, prob;
    
    subTimer.startTimer();
    for(int p=0; p<Np; p++){
        
        if(ants[p].startingPos < (int)gens.size()){
            ants[p].pos[ants[p].startingPos] = (mt.rand() < gens[ants[p].startingPos].getOutageRate()) ? 0 : 1;
        }else{
            ants[p].pos[ants[p].startingPos] = (mt.rand() < lines[ants[p].startingPos-gens.size()].getOutageRate()) ? 0 : 1;
        }
        curPos	= ants[p].startingPos;
        nextPos = (curPos + 1) % Nd;

        for(int i=0; i<Nd; i++){
            r 	 = mt.rand();
            prob = pMones[i];
            if(r < prob){ ants[p].pos[nextPos] = 0;}
            else        { ants[p].pos[nextPos] = 1;}
            curPos 	= (curPos + 1) % Nd;
            nextPos = (curPos + 1) % Nd;
        } //End for each Dimension
    } //End for each Ant
    subTimer.stopTimer();
    updatePopTime += subTimer.getElapsedTime();
}

void ACOPruner::evaluateFitness(){
    
    double localBestFitness = bestFitness;
    
    subTimer.startTimer();

    #pragma omp parallel for
    for(int p=0; p<Np; p++){
        ants[p].fitness	= EvaluateSolution(ants[p].pos);
    }

    for(int p=0; p<Np; p++){                
        if(ants[p].fitness > bestFitness){
            bestFitness	= ants[p].fitness;
            bestIndex 	= p;
        }
    }
    subTimer.stopTimer();
    fitnessTime += subTimer.getElapsedTime();
}

void ACOPruner::updatePheromones(){
    subTimer.startTimer();
    for(int p=0; p<Np; p++){
        for(int i=0; i<Nd; i++){
            if(ants[p].pos[i] == 1) { pMones[i] += (1-rho) * deltaT * ants[p].fitness/bestFitness;}
            else                    { pMones[i] -= (1-rho) * deltaT * ants[p].fitness/bestFitness;}
        }
    }
    subTimer.stopTimer();
    updatePMoneTime += subTimer.getElapsedTime();
}
void ACOPruner::Prune(MTRand& mt){
    timer.startTimer();

    #ifdef _MSC_VER
        bestFitness = -std::numeric_limits<double>::infinity();
    #else
        bestFitness = -INFINITY;
    #endif
    bestIndex = 0;

    initPheromones();
    initPopulation(mt);

    totalIterations = 0;
    while(!isConverged()){
        updateSolutions(mt);
        evaluateFitness();
        updatePheromones();
        totalIterations++;
    }
    timer.stopTimer();
    pruningTime = timer.getElapsedTime();

    //cout    << setprecision(6) 
    //        << initPMoneTime  << " "
    //        << initPopTime     << " "
    //        << updatePopTime   << " "
    //        << updatePMoneTime << " "
    //        << fitnessTime     << "\n";
}

ACOPruner::~ACOPruner() {
    ants.clear();
    pMones.clear();
}

void ACOPruner::clearVectors(){
    Pruner::clearVectors();
    ants.clear();
    pMones.clear();
}

bool ACOPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double totalHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(unsigned int i=0; i<ants.size(); i++){
            avgFitness += ants[i].fitness;
        }
        avgFitness /= ((double)ants.size());

        for(unsigned int i=0; i<ants.size(); i++){
            stdDev += pow(ants[i].fitness - avgFitness,2);
        }
        stdDev /= (ants.size()-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        localHammingDist = 0;
        for(unsigned int i=0; i<ants.size(); i++){
            for(unsigned int j=0; j<ants.size(); j++){
                if(i != j){
                    localHammingDist = 0;
                    for(unsigned int k=0; k<ants[i].pos.size(); k++){
                        localHammingDist +=  abs(ants[i].pos[k] - ants[j].pos[k]);
                    }
                    totalHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(totalHammingDist/ants.size());
    }

    return retValue;
}
